package tracer

import (
	"bytes"
	"runtime"
	"strconv"
	"time"

	"github.com/coocood/freecache"
)

//用来保存traceID
var traceCache *freecache.Cache
var timeout = 60 //单位秒

func init() { //用1M来存事务ID,正常可以存1.6W个事务ID,正常来说,一分钟的病发量不可能这么多
	traceCache = freecache.NewCache(1024 * 1024)
}
func SetDefaultTimeout(second int) {
	if second > 0 {
		timeout = second
	}
}
func Goid() string {
	b := originId()
	if v, e := traceCache.Get(b); e == nil {
		return string(v)
	} else {
		return string(b)
	}
}

//获取事务ID,当发起对外请求是,调用该方法获取
func TraceID() string {
	b := originId()
	if v, e := traceCache.Get(b); e == nil {
		return string(v)
	} else {
		trace := traceID() + string(b)
		traceCache.Set(b, []byte(trace), timeout)
		return trace
	}
}
func originId() []byte {
	b := make([]byte, 64)
	b = b[:runtime.Stack(b, true)]
	b = bytes.TrimPrefix(b, []byte("goroutine "))
	b = b[:bytes.IndexByte(b, ' ')]
	return b
}
func traceID() string {
	se := time.Now().Unix()
	return strconv.FormatInt(se, 10)
}
func SetGoid(traceId string) {
	traceCache.Set(originId(), []byte(traceId), timeout)
}

//清除当前traceID
func Remove() {
	traceCache.Del(originId())
}

package network

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"strings"
	"time"

	"gitlab.com/hadbean/utils/logger"
	"gitlab.com/hadbean/utils/tracer"
)

var (
	TIME_OUT = 10 * time.Second //默认超时时间
	SKIP_TLS = true             //是否跳过https验证
)

func init() {

}

type Client struct {
	*http.Client
	Transport  http.RoundTripper
	Jar        http.CookieJar
	Timeout    time.Duration
	StringResp func(resp *http.Response) *[]byte
}

var DefaultClient = &Client{Client: http.DefaultClient}

func (cli *Client) Do(req *http.Request) (resp *http.Response, err error) {
	if cli.Timeout > 0 {
		cli.Client.Timeout = cli.Timeout
	}
	if cli.Transport != nil {
		cli.Client.Transport = cli.Transport
	}
	if cli.Jar != nil {
		cli.Client.Jar = cli.Jar
	}
	return Do(req, cli.Client)
}

func (cli *Client) Post(url, contentType string, body io.Reader) (resp *http.Response, err error) {
	req, err := NewRequest("POST", url, body)
	if err != nil {
		return nil, err
	}
	return cli.Do(req)
}
func (cli *Client) PostForm(url string, body io.Reader) (resp *http.Response, err error) {
	req, err := NewRequest("POST", url, body)
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	return cli.Do(req)
}
func (cli *Client) Get(url string) (resp *http.Response, err error) {
	req, err := NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}
	return cli.Do(req)
}
func Post(url, contentType string, body io.Reader) (resp *http.Response, err error) {
	req, err := http.NewRequest("POST", url, body)
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", contentType)
	return Do(req, nil)
}
func toBytes(data interface{}) (io.Reader, error) {
	if data == nil {
		return nil, nil
	}
	switch v := data.(type) {
	case []byte:
		{
			if len(v) == 0 {
				return nil, nil
			}
			return bytes.NewReader(v), nil
		}
	case string:
		{
			if len(v) == 0 {
				return nil, nil
			}
			return strings.NewReader(v), nil
		}
	default:
		{
			if bs, err := json.Marshal(&data); err != nil {
				return nil, err
			} else {
				return bytes.NewReader(bs), nil
			}
		}
	}
}

//data 类型支持 Struct ,或者转成json的字符串或者[]byte
func PostJson(url string, data interface{}) (resp *http.Response, err error) {
	v, err := toBytes(data)
	if err != nil {
		return nil, err
	}
	return Post(url, "application/json", v)
}
func NewRequest(method, url string, body io.Reader) (req *http.Request, err error) {
	req, err = http.NewRequest(method, url, nil)
	if err != nil {
		return nil, err
	} else {
		handleRequest(req)
	}
	return
}

func Get(url string) (resp *http.Response, err error) {
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}
	return Do(req, nil)
}

func Do(req *http.Request, cli *http.Client) (resp *http.Response, err error) {
	handleRequest(req)
	start := time.Now()
	var reqBody []byte
	if req.Body != nil {
		reqBody, err = ioutil.ReadAll(req.Body)
		req.Body = ioutil.NopCloser(bytes.NewBuffer(reqBody))
	}
	defer func() {
		var layout = logger.LogLayout{}
		layout.Cost = time.Since(start)
		layout.Path = req.URL.String()
		layout.Query = req.URL.RawQuery
		layout.Method = req.Method
		if len(reqBody) > 0 {
			layout.Body = string(reqBody)
		}
		layout.Traceid = req.Header.Get("x-trace-id")
		layout.Time = start
		if err != nil {
			layout.Error = err.Error()
			layout.StatusCode = 400
		} else {
			if resp != nil {
				layout.StatusCode = resp.StatusCode
				var cnt []byte
				cnt, _ = ioutil.ReadAll(resp.Body)
				resp.Body = ioutil.NopCloser(bytes.NewBuffer(cnt))
				layout.Resp = string(cnt)
			}
		}
		s := fmt.Sprintf("%3d %13v %s [body]%s [response]%s [path]%s %s",
			layout.StatusCode,
			layout.Cost,
			layout.Method,
			layout.Body,
			layout.Resp,
			layout.Path,
			layout.Error,
		)
		logger.Log.Debug(s)
		s2 := fmt.Sprintf("%3d %13v %s %s %s",
			layout.StatusCode,
			layout.Cost,
			layout.Method,
			layout.Path,
			layout.Error,
		)
		logger.Log.Info(s2)
	}()
	if cli == nil {
		cli := http.Client{Timeout: TIME_OUT}
		if SKIP_TLS && req.URL.Scheme == "https" { //跳过https验证
			cli.Transport = &http.Transport{TLSClientConfig: &tls.Config{InsecureSkipVerify: true}}
		}
		resp, err = cli.Do(req)
	} else {
		resp, err = cli.Do(req)
	}
	return
}
func handleRequest(req *http.Request) {
	req.Header.Set("x-trace-id", tracer.TraceID())
}

func PostForm(url string, data url.Values) (resp *http.Response, err error) {
	return Post(url, "application/x-www-form-urlencoded", strings.NewReader(data.Encode()))
}

func PostFile(url string, file []byte) (resp *http.Response, err error) {
	return Post(url, "multipart/form-data", bytes.NewBuffer(file))
}

func PostFileWithPath(url, path string) (resp *http.Response, err error) {
	file, err := os.ReadFile(path)
	if err != nil {
		return nil, err
	}
	return Post(url, "multipart/form-data", bytes.NewBuffer(file))
}

func Put(url string, body interface{}) (resp *http.Response, err error) {
	v, err := toBytes(body)
	if err != nil {
		return nil, err
	}
	req, err := NewRequest("PUT", url, v)
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", "application/json")
	return Do(req, nil)
}

func Delete(url string, body interface{}) (resp *http.Response, err error) {
	v, err := toBytes(body)
	if err != nil {
		return nil, err
	}
	req, err := NewRequest("DELETE", url, v)
	if err != nil {
		return nil, err
	}
	return Do(req, nil)
}

func ToString(resp *http.Response) (cnt []byte, statusCode int, err error) {
	if resp == nil {
		return nil, 0, errors.New("返回为空")
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, resp.StatusCode, err
	} else {
		return body, resp.StatusCode, err
	}
}
func BindJson(resp *http.Response, target interface{}) (err error) {
	if resp == nil {
		return errors.New("response is nil")
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	err = json.Unmarshal(body, target)
	return
}

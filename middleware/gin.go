package middleware

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/hadbean/utils/logger"
)

//初始化总路由
func Routers() *gin.Engine {
	gin.SetMode(gin.ReleaseMode)

	r := gin.New()
	r.Use(DefaultGinLogger(), GinErrRecovery(true))
	logGroup := r.Group("/log")
	logGroup.GET("/mode/debug", func(ctx *gin.Context) {
		cfg := logger.GetLogCfg()
		cfg.LogLevel = "debug"
		logger.InitLogger(&cfg)
	})
	logGroup.GET("/mode/info", func(ctx *gin.Context) {
		cfg := logger.GetLogCfg()
		cfg.LogLevel = "info"
		logger.InitLogger(&cfg)
	})
	return r
} //初始化总路由
func RoutersWithLogApi() *gin.Engine {
	gin.SetMode(gin.ReleaseMode)

	r := gin.New()
	r.Use(DefaultGinLogger(), GinErrRecovery(true))
	logGroup := r.Group("/log")
	logGroup.GET("/mode/debug", func(ctx *gin.Context) {
		cfg := logger.GetLogCfg()
		cfg.LogLevel = "debug"
		logger.InitLogger(&cfg)
	})
	logGroup.GET("/mode/info", func(ctx *gin.Context) {
		cfg := logger.GetLogCfg()
		cfg.LogLevel = "info"
		logger.InitLogger(&cfg)
	})
	return r
}
